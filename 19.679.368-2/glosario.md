# _Glosario:_
**a. Control de versiones (VC):** Sistema que controla las versiones de los diferentes archivos o conjuntos de archivos de un proyecto para accederlos y/o recuperarlos a traves del tiempo.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)
**b. Control de versiones distribuido (DVC):** Sistema que brinda una solución a los problemas presentes al trabajar en un servidor, a traves del cual colaboran otros sistemas. Un problema sucede cuando el servidor deja de funcionar, para esto, el DVC permite recuperar el repositorio perdido.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-del-Control-de-Versiones)
**c. Repositorio remoto y Repositorio local:** Repositorio remoto son aquellas versiones de un proyecto que estan almacenadas en algun lugar de internet. Se pueden tener varios de estos, los cuales pueden ser de escritura y/o lectura segun los permisos relacionados, y asi de la misma manera gestionarlos, mandando (push) y recibiendo (pull) datos de ellos cuando necesites compartir cosas. Un repositorio local es aquel que reside en el servidor local.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
**d. Copia de trabajo / Working Copy:** Una copia de trabajo es cada una de las versiones del proyecto, las cuales estan comprimidas en los directorios de git.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
**e. Área de Preparación / Staging Area:** El área de preparación es un archivo comprimido en git, el cual contiene información de lo que ira en la proxima confirmación, por lo cual tambien es llamado índice aunque se utilice más el termino área de preparación.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
**f. Preparar Cambios / Stage Changes:** Para preparar cambios se marca un archivo ya modificado en su versión actual para que este en la siguiente confirmación.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Empezando-fundamentos-de-git)
**g. Confirmar cambios / Commit Changes:** Confirmar cambios asegura que los datos/archivos preparados, se guarden en la base de datos de manera segura.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Fundamentos-de-Git)
**h. Commit:** Commit pone una firma/mensaje para que cada una de las versiones realizadas puedan ser identificadas.
[Para mayor información, click aquí](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)
**i. clone:** Clone obtiene una copia de un repositorio Git existente creando un directorio llamado "grit", para inicializar un directorio .git en su interior, descargando toda la información de ese repositorio, y sacando una copia de trabajo de la última versión.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Obteniendo-un-repositorio-Git)
**j. pull:** Pull obtiene todos los archivos de un repositorio remoto, descargando solo los que son diferentes en version o no existen en el repositorio local.
[Para mayor información, click aquí](https://es.stackoverflow.com/questions/245/cu%C3%A1l-es-la-diferencia-entre-pull-y-fetch-en-git)
**k. push:** Push es un comando que sube los cambios realizados en tu ambiente de trabajo a una rama de trabajo interna o remota.
[Para mayor información, click aquí](https://es.stackoverflow.com/questions/28344/cu%C3%A1l-es-la-diferencia-entre-commit-y-push-en-git)
**l. fetch:** Fech recupera todos los datos que no tengas de tu repositorio remoto, referenciando a todas las ramas del repositorio remoto, que puedes unir o inspeccionar en cualquier momento.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Trabajando-con-repositorios-remotos)
**m. merge:** Merge es el comando utilizado para fusionar ramas o branches.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)
**n. status:** Status es el comando para determinar en que estados estan los archivos, mostrando las diferencias entre el ultimo commit del repositorio remoto y local.
[Para mayor información, click aquí](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Guardando-cambios-en-el-Repositorio)
**o. log:** El comando log lista los commit hechos en tu proyecto o repositorio en orden cronológico inverso. 
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Viendo-el-hist%C3%B3rico-de-confirmaciones)
**p. checkout:** Es un comando que permite crear nuevas ramas en un proyecto cuando sea necesario, y moverse entre ellas en un solo paso.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-Procedimientos-b%C3%A1sicos-para-ramificar-y-fusionar)
**q. Rama / Branch:** Una rama en git es una linea independiente de desarrollo que se utilizan para probar funcionalidades.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Ramificaciones-en-Git-%C2%BFQu%C3%A9-es-una-rama%3F)
**r. Etiqueta / Tag:** Loa tag son utilizados para etiquetar momentos historicos o versiones especificas.
[Para mayor información, click aquí](https://git-scm.com/book/es/v1/Fundamentos-de-Git-Creando-etiquetas)